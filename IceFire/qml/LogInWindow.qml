import QtQuick
import QtQuick.Controls


Dialog {
    id:control

    property color bgColor: "#1d5652"
    property int radius: 4

    background: Rectangle{
        id:bg
        anchors.fill: parent
        color: bgColor
        radius: control.radius
    }

    contentItem: Rectangle{
        id:cd
        anchors.fill: parent
        color: "transparent"
        radius: control.radius
        Item{
            id:headerImage
            width: 120
            height: 120
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 50
            Rectangle{
                anchors.fill: parent
                color: "#606060"
                radius: 60
            }
        }
        Rectangle{
            id:account
            anchors.top: headerImage.bottom
            anchors.topMargin: 40
            anchors.left: parent.left
            anchors.leftMargin: 48
            width: 195
            height: 30
            radius: control.radius
            color: "#606060"
            XLineText{
                anchors.fill: parent
                bgColor: parent.color
                radius: parent.radius
                placeholderText:"请输入账号"
                borderColor: "skyblue"
                onPressed: {
                    console.log("按下")
                }
                onAccepted: {
                    console.log("回车")
                }
            }
        }
        Rectangle{
            id:password
            anchors.top: account.bottom
            anchors.topMargin: 25
            anchors.left: account.left
            width: 195
            height: 30
            radius: control.radius
            color: "#606060"
            XLineText{
                anchors.fill: parent
                bgColor: parent.color
                borderColor: "skyblue"
                radius: parent.radius
                placeholderText:"请输入密码"
                onPressed: {
                    console.log("按下")
                }
                onAccepted: {
                    console.log("回车")
                }
            }
        }

        Rectangle{
            height: 20
            width: parent.width
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: height/2
            color: bgColor
            Label{
                font.pixelSize: 12
                color: "red"
                text: "忘记密码"
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 10
                clip: true
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        console.info("忘记密码")
                    }
                }
            }
            Label{
                font.pixelSize: 12
                color: "red"
                text: "注册"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 10
                clip: true
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        console.info("注册")
                    }
                }
            }
        }

        Rectangle{
            id:left_btn
            width: 65
            height: 30
            anchors.top: password.bottom
            anchors.left: password.left
            anchors.topMargin: 38
            radius: control.radius*2
            color: "#331c8f"
            Text{
                anchors.centerIn: parent
                font.pixelSize: 12
                color: "red"
                text:"取消"
            }
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    console.log("取消登录")
                    control.close()
                }
            }
        }
        Rectangle{
            id:right_btn
            width: 65
            height: 30
            anchors.top: password.bottom
            anchors.right: password.right
            anchors.topMargin: 38
            color: "#331c8f"
            radius: control.radius*2
            Text{
                anchors.centerIn: parent
                font.pixelSize: 12
                color: "red"
                text:"登录"
            }
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    console.log("点击登录")
                    control.close()
                }
            }
        }
    }

}
