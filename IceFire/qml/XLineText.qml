import QtQuick
import QtQuick.Templates as T
import QtQuick.Controls.impl
import QtQuick.Controls.Fusion
import QtQuick.Controls.Fusion.impl
import QtQuick.Controls

TextField{
    id:control
    implicitHeight: 30
    implicitWidth: 150
    property color bgColor: "black"
    property color borderColor: "white"
    property int radius: 3
    //property alias placeholderText: control.placeholderText
    leftInset:0
    bottomInset:0
    topInset:0
    rightInset:0
    placeholderText:"请输入"
    background: Rectangle{
        anchors.fill: parent
        color: bgColor
        border.color: borderColor
        radius: control.radius
    }
}
